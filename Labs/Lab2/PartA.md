[LAB2 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab2/setup.md)

### Part A - OO Python GPIO Driver ###
**Synopsis:** A good architectural design starts with partitioning the system
into components. One such component in our IoT system will be a GPIO interface
driver for the 3 LEDs and one pushbutton switch we have defined. We will design
methods for each of the actions and then a unit test to exercise each of these
methods with the target hardware on our RPi breadboard.

#### Step A1: Building a Simple GPIO Python Driver
**Objective:** We'll design a simple object oriented Python module to handle I/O
for our project.  Let's start by creating a file called ```gpio.py``` and
importing libraries we will need for handling Raspberry PI GPIO.
``` python
import RPi.GPIO as GPIO
```
Next we will define each of the pins we will use for the LEDs and SWITCH.
``` python
LED1_PIN    = 18    # cobbler pin 12 (GPIO18)
LED2_PIN    = 13    # cobbler pin 33 (GPIO13)
LED3_PIN    = 23    # cobbler pin 16 (GPIO23)
SWITCH_PIN  = 27    # cobbler pin 7  (GPIO27)
```
Next we'll declare the name of the class we will use to instantiate our GPIO
object from our web server code.
```python
class PiGpio(object):
    """Raspberry Pi Internet 'IoT GPIO'."""
```
We need to define what happens when we initialize the driver in an init method.
``` python
def __init__(self):
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(LED1_PIN, GPIO.OUT)      # RED LED as output
    GPIO.setup(LED2_PIN, GPIO.OUT)      # GRN LED as output
    GPIO.setup(LED3_PIN, GPIO.OUT)      # BLU LED as output
    GPIO.setup(SWITCH_PIN, GPIO.IN)     # Switch as input w/pu
    GPIO.setup(SWITCH_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
```

Next we need a GPIO method to handle reading of the input switch pin. Notice
that we will invert the logic of the pin due to the momentary switch circuit
that grounds out the input upon pressing the switch down.
``` python
def read_switch(self):
    """Read the switch state."""
    switch = GPIO.input(SWITCH_PIN)
    # invert because of active low momentary switch
    if (switch == 0):
        switch=1
    else:
        switch=0
    return switch
```

And finally, we need ```getter``` and ```setter``` methods for the LED control
for each of the 3 LEDs we have defined.
``` python
# set the particular LED to ON or OFF
def set_led(self, led, value):
    """Change the LED to the passed in value, '1 ON or '0' OFF."""
    if(led == 1):
        GPIO.output(LED1_PIN, value)
    if(led == 2):
        GPIO.output(LED2_PIN, value)
    if(led == 3):
        GPIO.output(LED3_PIN, value)

# get the state of an LED
def get_led(self, led):
    """Return the state value of the LED, '1' ON or '0' OFF."""
    if(led == 1):
        return GPIO.input(LED1_PIN)
    if(led == 2):
        return GPIO.input(LED2_PIN)
    if(led == 3):
        return GPIO.input(LED3_PIN)
```

#### Step A2: GPIO Driver Unit Test
**Objective:** In order to thoroughly test our ```gpio.py``` driver we need some
low level unit test code that can be run from the command line shell as follows
(along with its corresponding output):
``` sh
pi$ ./gpio_test.py

============ Switch: 0 ============
LED 1 ON (RED)
LED1: 1
LED2: 0
LED3: 0

LED 2 ON (GRN)
LED1: 1
LED2: 1
LED3: 0

LED 3 ON (BLU)
LED1: 1
LED2: 1
LED3: 1

LED 1 OFF (RED)
LED1: 0
LED2: 1
LED3: 1

LED 2 OFF (GRN)
LED1: 0
LED2: 0
LED3: 1

LED 3 OFF (BLU)
LED1: 0
LED2: 0
LED3: 0
```
We will start by importing our GPIO driver and the time function we will need
for some time delays.  Note also the inclusion of a "she-bang" which will be
used to turn this file into an executable by telling the Linux shell where to
find the Python program at ```/usr/bin/python```.

```python
#!/usr/bin/python
import time
from gpio import PiGpio
```

In order to test the driver, we will need to instantiate it.
```python
# create an instance of the pi gpio driver.
pi_gpio= PiGpio()
```

Next we will simply run a forever while loop to test each of the methods
```python
# Blink the LEDS forever.
print('Blinking all my LEDs in sequence (Ctrl-C to stop)...')
while True:
# Get the current switch state and print
    switch = pi_gpio.read_switch()
    print('\n============ Switch: {0} ============'.format(switch))

    print('\nLED 1 ON (RED)')
    pi_gpio.set_led(1,True)
    print('LED1: {0}'.format(pi_gpio.get_led(1)))
    print('LED2: {0}'.format(pi_gpio.get_led(2)))
    print('LED3: {0}'.format(pi_gpio.get_led(3)))
    time.sleep(1.0)

    print('\nLED 2 ON (GRN)')
    pi_gpio.set_led(2,True)
    print('LED1: {0}'.format(pi_gpio.get_led(1)))
    print('LED2: {0}'.format(pi_gpio.get_led(2)))
    print('LED3: {0}'.format(pi_gpio.get_led(3)))
    time.sleep(1.0)

    print('\nLED 3 ON (BLU)')
    pi_gpio.set_led(3,True)
    print('LED1: {0}'.format(pi_gpio.get_led(1)))
    print('LED2: {0}'.format(pi_gpio.get_led(2)))
    print('LED3: {0}'.format(pi_gpio.get_led(3)))
    time.sleep(1.0)


    print('\nLED 1 OFF (RED)')
    pi_gpio.set_led(1,False)
    print('LED1: {0}'.format(pi_gpio.get_led(1)))
    print('LED2: {0}'.format(pi_gpio.get_led(2)))
    print('LED3: {0}'.format(pi_gpio.get_led(3)))
    time.sleep(1.0)

    print('\nLED 2 OFF (GRN)')
    pi_gpio.set_led(2,False)
    print('LED1: {0}'.format(pi_gpio.get_led(1)))
    print('LED2: {0}'.format(pi_gpio.get_led(2)))
    print('LED3: {0}'.format(pi_gpio.get_led(3)))
    time.sleep(1.0)

    print('\nLED 3 OFF (BLU)')
    pi_gpio.set_led(3,False)
    print('LED1: {0}'.format(pi_gpio.get_led(1)))
    print('LED2: {0}'.format(pi_gpio.get_led(2)))
    print('LED3: {0}'.format(pi_gpio.get_led(3)))
    time.sleep(1.0)
```
In order to run this unit test from the command line it will need to be made
executable.
```sh
pi$ chmod +x gpio_test.py
```
Then it can be run using the "./" method of execution from the command line.

[LAB2 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab2/setup.md)
