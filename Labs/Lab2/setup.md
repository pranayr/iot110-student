[IOT STUDENT HOME](https://gitlab.com/iot110/iot110-student/blob/master/README.md)

## Setting up Lab2

### Objectives
For this lab we will continue building on our IoT webapp by adding basic GPIO
capability in a layered architectural manner.  We will build an object oriented
Python ```gpio.py``` driver as well as a unit test ```gpio_test.py``` for the
driver code.

We will also build a simple introduction to API design that provides a hands-on
approach to designing *HTTP get/post* methods within the Flask webserver
framework.

Next we'll build a simple ```index.html``` page that has three
buttons for toggling the LED states as well as an indicator for the state of
the switch.  This webpage will also have a small amount of Javascript (jQuery) to
handle pushbutton events.


The various steps of this lab are summarized as:
* [PART A](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab2/PartA.md) Design an object oriented Python GPIO driver and unit test.
* [PART B](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab2/PartB.md) Build a Flask webserver application that shows how to build HTTP GET/POST API.
* [PART C](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab2/PartC.md) Expand the API webserver design to interface to the GPIO module methods.
* [PART D](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab2/PartD.md) Build our GPIO IoT Webapp and show how to interface it as a template to Flask.
* [PART E](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab2/PartE.md) Code each of the Javascript events that need handling from button clicks
* [PART F](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab2/PartF.md) Fine tune the Javascript logic to handle startup conditions
