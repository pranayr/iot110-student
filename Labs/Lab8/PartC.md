[LAB8 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab8/setup.md)

## Part C - Create MQTT Python Test Code
**Synopsis:** In this part, we will code a number client entities to simulate
sensors, test real sensors from the SenseHat, create a MQTT listener that also
redirects payloads and sets up the framework that will be used for eventually
connecting to the Azure IoT Suite cloud platfrom.

## Objectives
* apt-get MQTT libraries
* Install MQTT.fx on development host
* Configure and establish user and password

### Step C1: Create an MQTT Listener
As usual, we will need to import a few tools.  We will need som
```python
#!/usr/bin/python
import time
import paho.mqtt.client as paho
import json
import ast

localBroker = "iot8e3c"     # Local MQTT broker
localPort   = 1883          # Local MQTT port
localUser   = "pi"          # Local MQTT user
localPass = "raspberry"     # Local MQTT password
localTopic = "iot/sensor"   # Local MQTT topic to monitor
cloudTopic = "iot/azure"    # publish to simulated Azure topic 
localTimeOut = 120          # Local MQTT session timeout

```    

### Step C2: Event Callback for On Connect
```python
# The callback for when a CONNACK message is received from the broker.
def on_connect(client, userdata, flags, rc):
    print("CONNACK received with code %d." % (rc))
    mqttc.subscribe(localTopic, 0)
```

### Step C3: Event Callback for On Message
```python
def on_message(mosq, obj, msg):
    global message
    print("Topic Rx:" + msg.topic + " QoS:" + str(msg.qos) + "\n")
    clean_json = ast.literal_eval(msg.payload)
    print ("Payload: ")
    # import pdb; pdb.set_trace()
    print json.dumps(clean_json , indent=4, sort_keys=True)
    # message = msg.payload
    # This is where we will develop client connect to  Azure IoT Suite
    mqttc.publish(cloudTopic,msg.payload, 1);
```
### Step C4: MQTT Listener startup Main
```python
print('Establish MQTT Broker Connection')
# Setup to listen on topic`
mqttc = paho.Client()
mqttc.on_connect = on_connect
mqttc.on_message = on_message
mqttc.connect(localBroker, localPort, localTimeOut)
print('MQTT Listener Loop <ctl-C> to break...')
mqttc.loop_forever()
```

### Step C5: Create an MQTT Test Client (part 1)
```python
#!/usr/bin/python
import time
import socket
import paho.mqtt.client as paho
from sense import PiSenseHat

myHostname = 'iot8e3c'

localBroker = "iot8e3c"		# Local MQTT broker
localPort   = 1883			# Local MQTT port
localUser   = "pi"			# Local MQTT user
localPass = "raspberry"		# Local MQTT password
localTopic = "iot/sensor"	# Local MQTT topic to monitor
localTimeOut = 120			# Local MQTT session timeout

# The callback for when a CONNACK message is received from the broker.
def on_connect(client, userdata, flags, rc):
    print("CONNACK received with code %d." % (rc))

# The callback for when a PUBLISH message is received from the broker.
def on_message(client, userdata, msg):
    print (string.split(msg.payload))

# display one row of blue LEDs (just for fun)
def displayLine(row):
    blue = (0, 0, 255)
    for i in range(0,8):
        x = i % 8
        y = (i / 8) + row
        # print("set pixel x:%d y:%d" % (x,y))
        pi_sense.set_pixel(x,y,blue)
        time.sleep(0.02)

```
### Step C6: Create an MQTT Test Client (part 2)
```python
# Create a sense-hat object
pi_sense = PiSenseHat()
print('PI SenseHat Object Created')

# Get hostname
## Get my machine hostname
# import pdb; pdb.set_trace()
if socket.gethostname().find('.') >= 0:
    hostname=socket.gethostname()
else:
    hostname=socket.gethostbyaddr(socket.gethostname())[0]

# Setup to Publish Sensor Data
mqttc = paho.Client()
mqttc.on_connect = on_connect
mqttc.on_message = on_message
mqttc.connect(localBroker, localPort, localTimeOut)

# initialize message dictionary
msg = {'topic':localTopic, 'payload':"", 'qos':0, 'retain':False }

pi_sense.clear_display()
# loop
print('Getting Sensor Data')
for i in range(1,9):
    print("SensorSet[%d]" % (i))
    displayLine(i-1)
    sensors = pi_sense.getAllSensors()

    sensors['host'] = hostname
    msg['payload'] = str(sensors)
    print("msg["+str(i)+"]:"+msg['payload'])

#   publish(topic, payload=None, qos=0, retain=False)
    mqttc.publish('iot/test', msg['payload'], 1)
    time.sleep(2.0)

print('End of MQTT Messages')
quit()
```

### Step C6: Create an MQTT Test Client (part 2)
```python

```

### Step C8: Modify our WebApp main.py setup
First, copy all of the code from Lab6 into Lab8.  Then make the following minor
changes to your ```main.py```.  Later, we will change the ```localBroker``` variable
to point to whatever platform is hosting Mosquitto the MQTT broker.
```python
# MQTT Configuration for local network
localBroker = "iot8e3c"		# Local MQTT broker
localPort   = 1883			# Local MQTT port
localUser   = "pi"			# Local MQTT user
localPass = "raspberry"		# Local MQTT password
localTopic = "iot/sensor"	# Local MQTT topic to monitor
localTimeOut = 120			# Local MQTT session timeout

# Setup to Publish Sensor Data
mqttc = paho.Client()
mqttc.on_connect = on_connect
mqttc.on_message = on_message
mqttc.connect(localBroker, localPort, localTimeOut)
```

### Step C9: Modify our WebApp main.py SSE route and method
```python
# =========================== Endpoint: /myData ===============================
# read the sensor values by GET method from curl for example
# curl http://iot8e3c:5000/myData
# -----------------------------------------------------------------------------
@app.route('/myData')
def myData():
    def get_values():
        while True:
            # return the yield results on each loop, but never exits while loop
            data_payload = get_sensor_values()
            yield('data: {0}\n\n'.format(data_payload))
            print("MQTT Topic:"+localTopic, data_payload)
            mqttc.publish(localTopic,data_payload)
            time.sleep(2.0)
    return Response(get_values(), mimetype='text/event-stream')
# ============================== API Routes ===================================
```


### Step C10: Modify Sense.py to include LED display

Add the following two functions from line 24 on the sense.py from Lab6
```python
    # pixel display
    def set_pixel(self,x,y,color):
    # red = (255, 0, 0)
    # green = (0, 255, 0)
    # blue = (0, 0, 255)
        self.sense.set_pixel(x, y, color)

    # clear pixel display
    def clear_display(self):
        self.sense.clear()
```


# STEPS TO MODIFY in ```main.py```
### STEP C11: Initial Import structure
```python 
#!/usr/bin/python
import time
import datetime
from sense import PiSenseHat
import paho.mqtt.client as paho
from flask import *
```

### STEP C12: MQTT Callbacks
```python
# ============================= MQTT Callbacks ================================
# The callback for when a CONNACK message is received from the broker.
def on_connect(client, userdata, flags, rc):
    print("CONNACK received with code %d." % (rc))

# The callback for when a PUBLISH message is received from the broker.
def on_message(client, userdata, msg):
    print (string.split(msg.payload))
# ============================= MQTT Callbacks ================================
```
### STEP C13: LOGIN CONSTANTS and SETUP TO PUBLISH
```python
# MQTT Configuration for local network
localBroker = "iot8e3c"		# Local MQTT broker
localPort   = 1883			# Local MQTT port
localUser   = "pi"			# Local MQTT user
localPass = "raspberry"		# Local MQTT password
localTopic = "iot/sensor"	# Local MQTT topic to monitor
localTimeOut = 120			# Local MQTT session timeout

# Setup to Publish Sensor Data
mqttc = paho.Client()
mqttc.on_connect = on_connect
mqttc.on_message = on_message
mqttc.connect(localBroker, localPort, localTimeOut)
```
### STEP C14: Addition of Publish in our SSE Loop
```python
# =========================== Endpoint: /myData ===============================
# read the sensor values by GET method from curl for example
# curl http://iot8e3c:5000/myData
# -----------------------------------------------------------------------------
@app.route('/myData')
def myData():
    def get_values():
        while True:
            # return the yield results on each loop, but never exits while loop
            data_payload = get_sensor_values()
            yield('data: {0}\n\n'.format(data_payload))
            print("MQTT Topic:"+localTopic, str(data_payload))
            mqttc.publish(localTopic,str(data_payload))
            time.sleep(2.0)
    return Response(get_values(), mimetype='text/event-stream')
```

[PART D](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab8/PartD.md) Test Multiple Pub/Sub Connections

[LAB8 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab8/setup.md)
